﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scroller : MonoBehaviour {

    public float speed = 1.0f;

    bool game_end;
    float end_pt;

	// Use this for initialization
	void Start () {
        game_end = false;
        float ratio = (float)16 / 9;
        end_pt = (transform.localScale.x - (transform.localScale.y * ratio)) * 0.5f;
        Debug.Log(end_pt);
        //transform.Translate(new Vector3(-end_pt, 0, 0));

	}
	
	// Update is called once per frame
	void Update () {
        
        if (game_end == false){
            float time = Time.time * speed;
            transform.Translate(new Vector3(time, 0, 0));
        }

        if (transform.position.x > end_pt) game_end = true;

	}
}
