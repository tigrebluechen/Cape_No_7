﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MicrophoneController : MonoBehaviour {

    public bool useMicrophone;
    public AudioClip _audio;
    public string selectedDevice;
    Microphone device;

    public float updateStep = 1f;
    public int sampleDataLength = 1024;

    AudioSource audio_source;

    private float clipLoudness;
    private float currentUpdateTime = 0f;

    private float[] clipSampleData;

    // Use this for initialization

    private void Awake()
    {
        clipSampleData = new float[sampleDataLength];
    }

    void Start () {

        audio_source = GetComponent<AudioSource>();
        
        if (useMicrophone) {
            if(Microphone.devices.Length > 0){
                selectedDevice = Microphone.devices[0].ToString();
                audio_source.clip = Microphone.Start(selectedDevice, true, 100, AudioSettings.outputSampleRate);
            } else {
                useMicrophone = false;
            }
        } else {
            audio_source.clip = _audio;
        }

        audio_source.Play();
	}
	
	// Update is called once per frame
	void Update () {
        currentUpdateTime += Time.deltaTime;
        if (currentUpdateTime >= updateStep)
        {
            currentUpdateTime = 0f;
            audio_source.clip.GetData(clipSampleData, audio_source.timeSamples); 
            //I read 1024 samples, which is about 80 ms on a 44khz stereo clip, beginning at the current sample position of the clip.
            clipLoudness = 0f;
            foreach (var sample in clipSampleData)
            {
                clipLoudness += Mathf.Abs(sample);
                //Debug.Log(sample);
            }
            clipLoudness /= sampleDataLength; //clipLoudness is what you are looking for
        }
        //Debug.Log(clipLoudness);
    }
}
