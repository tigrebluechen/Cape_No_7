﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class vid_player : MonoBehaviour {

    public bool play;

    VideoPlayer player;
	
	void Start () {
        player = GetComponent<VideoPlayer>();
        player.playbackSpeed = 2f;
        player.Play();
	}
	
	// Update is called once per frame
	void Update () {
        if(play) {
            player.Play();
        } else {
            player.Pause();
        }
	}
}
