﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class building_generator : MonoBehaviour {

    GameObject[] buildings;
    Vector3[] buildings_pos;
	// Use this for initialization
	void Start () {
        buildings = GameObject.FindGameObjectsWithTag("generate_point");
       
        buildings_pos = new Vector3[buildings.Length];

        for (int i = 0; i < buildings.Length; i++){
            buildings_pos[i] = buildings[i].GetComponent<Transform>().position;
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
