﻿Shader "Custom/testing_shader"
{
	Properties
	{
		//_MainTex ("Texture", 2D) = "white" {}
        _RowScale ("RowScale", Range (2.0,10.0)) = 5.0
	}
	SubShader
	{

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;
            uniform float _RowScale;

			fixed4 frag (v2f i) : SV_Target
			{
                float gray_scale = (i.uv.x * _RowScale) % 1.0;
                if(gray_scale < 0.2) gray_scale = 0.2;
                fixed4 col = fixed4(gray_scale,gray_scale,gray_scale,1.0);
				return col;
			}
			ENDCG
		}
	}
}
